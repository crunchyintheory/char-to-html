import * as vscode from 'vscode';
import entities from './entities';

export function activate(context: vscode.ExtensionContext) {

	console.log('char-to-html activated.');

	let disposable = vscode.commands.registerCommand('extension.replaceWithEntity', () => {
		const editor = vscode.window.activeTextEditor;
		if(!editor) {
			return;
		}
		let range: vscode.Range,
		originalText: string,
		text: string,
		code: string;
		editor.selections.forEach(x => {
			range = new vscode.Range(
				x.start,
				x.end
			),
			originalText = editor.document.getText(range),
			text = '';

			for(let i = 0; i < originalText.length; i++) {
				code = originalText.charCodeAt(i).toString();
				if(entities.has(code)) {
					text += `&${entities.get(code)};`;
				} else {
					text += `&#${code};`;
				}
			}

			editor.edit((builder) => {
				builder.replace(range, text);
			});
		});
	});

	context.subscriptions.push(disposable);
}

export function deactivate() {}
