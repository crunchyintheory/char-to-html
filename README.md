Converts any selected text to its corresponding HTML entity or entities.

Download:
 - Go to [Tags](https://gitlab.com/crunchyintheory/char-to-html/-/tags)
 - Download the "package" artifact
 
![](https://i.imgur.com/FdgYnso.png)